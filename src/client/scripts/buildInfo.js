define(['jquery'], function($) {
    function retrieveBuildInfo(branchName) {
        return $.getJSON('api/' + branchName);
    }

    return {
        getMasterInfo: function() {
            return retrieveBuildInfo('Radiator-Master');
        },

        getCandidateInfo: function() {
            return retrieveBuildInfo('Radiator-Candidate');
        },

        getProductionInfo: function() {
            return retrieveBuildInfo('Radiator-Prod');
        }
    }
});
