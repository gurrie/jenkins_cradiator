define(['jquery', 'buildInfo'], function($, buildInfo) {
    var $content = $('#content');

    function renderSections(masterBuildInfo, candidateBuildInfo, productionBuildInfo) {
        renderSection(masterBuildInfo[0]);
        renderSection(candidateBuildInfo[0]);
        renderSection(productionBuildInfo[0]);
    }

    function renderSection(buildInfo) {
        var sectionId = buildInfo.name.toLowerCase();
        var $section = $content.children("#" + sectionId);

        if ($section.length) {
            $section.remove();
        }
        var $header = renderHeader(buildInfo);
        var $failingJobs = renderJobInformation(buildInfo.failingJobs, 'failing', 'Failing Jobs');
        var $buildingJobs = renderJobInformation(buildInfo.buildingJobs, 'pending', 'Pending Jobs');
        var $buildSection = renderBuildSection($header, $failingJobs, $buildingJobs, sectionId, buildInfo);

        $('#content').append($buildSection);
    }

    function renderBuildSection($header, $failingJobs, $buildingJobs, sectionId, buildInfo) {
	    var $jobsSection = $('<div></div>');
	    $jobsSection.append($failingJobs);
        $jobsSection.append($buildingJobs);

        var $buildSection = $('<section></section>');
        $buildSection.append($header);
	    $buildSection.append($jobsSection);
        $buildSection.attr("id", sectionId);
        $buildSection.addClass('build-view');
        $buildSection.addClass(buildInfo.status);

        return $buildSection;
    }

    function renderHeader(buildInfo) {
        var $header = $('<h1></h1>');
        $header.text(buildInfo.name.toUpperCase());

        with(buildInfo) {
            if (failingJobs.length == 0 && buildingJobs.length == 0) {
                $header.addClass('large');
            }
        }

        return $header;
    }

    function renderJobInformation(jobs, jobType, title) {
	    var $container = $('<span></span>');
	    $container.addClass('jobs');
	    $container.addClass(jobType);

	if (jobs.length > 0) {
		$container.append('<h6>' + title + '</h6>');
		 
	        var $jobInformation = $('<ul></ul>');

	        jobs.forEach(function (jobName) {
        	    $jobInformation.append('<li>' + jobName + '</li>');
	        });

		$container.append($jobInformation);
	}

        return $container;
    }

    function clearPage() {
        $('#content').empty();
    }

    function renderPage() {
        var $masterPromise = buildInfo.getMasterInfo();
        var $candidatePromise = buildInfo.getCandidateInfo();
        var $productionPromise = buildInfo.getProductionInfo();

        $.when($masterPromise, $candidatePromise, $productionPromise).done(renderSections);
    }

    function startTimer() {
        setInterval(renderPage, 5000)
    }

    return {
        show: function() {
            clearPage();
            renderPage();
            startTimer();
        }
    }
});
