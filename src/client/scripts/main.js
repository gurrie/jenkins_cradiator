require.config({
    paths: {
        'jquery': 'lib/jquery'
    }
});

require(['pages/buildStatusPage', 'jquery'], function(buildStatusPage, $) {
    $.noConflict(true);

    buildStatusPage.show();
});
