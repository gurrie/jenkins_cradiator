var restify = require('restify');
var assert = require('assert');
var url = require('url');

module.exports = JenkinsClient;

function JenkinsClient(config) {
    var client = restify.createJsonClient({
        url: config.baseUrl
    });

    function getBuildStatus(failingJobs, buildingJobs) {
        if (failingJobs.length && buildingJobs.length) {
            return 'failing_inprogress';
        } else if (failingJobs.length) {
            return 'failing';
        } else if (buildingJobs.length) {
            return 'building';
        } else {
            return 'success'
        }
    }

    function getJobsForStatusColor(jobs, colorRegex) {
        var matchingJobs = [];

        jobs.forEach(function(job) {
            if (colorRegex.test(job.color)) {
                matchingJobs.push(job.name);
            }
        });

        return matchingJobs;
    }

    return {
        getBuildStatus: function(buildViewName, callback) {
            client.get('/alfred2/view/Services/view/' + buildViewName + '/api/json', function(err, req, res, buildViewInfo) {
                assert.ifError(err);

                var failingJobs = getJobsForStatusColor(buildViewInfo.jobs, /red/);
                var buildingJobs = getJobsForStatusColor(buildViewInfo.jobs, /_anime/);
                var buildStatus = getBuildStatus(failingJobs, buildingJobs);

                buildingJobs.forEach(function(buildingJob) {
                    var index = failingJobs.indexOf(buildingJob.name)

                    if (index > -1) {
                        failingJobs.splice(index, 1);
                    }
                });

                var buildInfoResponse = {
                    name: buildViewInfo.name,
                    status: buildStatus,
                    failingJobs: failingJobs,
                    buildingJobs: buildingJobs
                };

                callback(buildInfoResponse);
            });
        }
    }
}

