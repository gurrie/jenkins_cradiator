var restify = require('restify');

module.exports = RadiatorServer;

function RadiatorServer(name, jenkinsClient) {
    var server = null;

    return {
        start: function() {
            server = restify.createServer({
                name: name
            });

            server.get('/api/:viewName', function(req, res, next) {
                jenkinsClient.getBuildStatus(req.params.viewName, function(buildInfo) {
                    res.send(buildInfo);
                });
            });

            server.get(/\//, restify.serveStatic({
                directory: '../client',
                default: 'index.html'
            }));

            server.listen(9000, function() {
                console.log('%s listening at %s', server.name, server.url);
            });
        }
    }
}