var nconf = require('nconf');
var Server = require('./radiatorServer');
var JenkinsClient = require('./jenkinsClient');

loadConfig();
startServer();

function loadConfig() {
    nconf.file('./config.json');
}

function startServer() {
    var server = new Server(nconf.get('radiator:name'), new JenkinsClient(nconf.get('jenkins')));
    server.start();
}

